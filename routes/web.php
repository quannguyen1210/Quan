<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
	return view('home') ;
});
// 
Route::get('home1','Home@index');



// Route::get('update{id}/{name}/{age}/{address}','Home@update');

// Route::get('update1','Home@update1');

// Route::get('delete{id}','Home@delete');

 Route::get('form','Home@form');

 Route::get('admin1','Home@admin');

 Route::get('delete{id}','Home@delete');

// Route::get('show/{id}','Home@show');


// Route::get('dashboard','Home@dashboard');

//Route::get('table',function() {
// 	return view('dashboard/table');
// });

 








Route::get('logout', 'Auth\LoginController@logout');

 // Route::get('insert/{hello}/{name}/{job}/{txtbg}/{firsttitle}/{lasttitle}','Home@insert');
Route::post('insert','Home@insert');

 Route::get('updateinfo{id}/{home}/{aboutme}/{profile}/{getintouch}','Home@updateinfo');

 Route::get('delete{id}','Home@delete');

Route::get('table','Home@infomation')->name('table');

route::get('edituser','Home@edituser')->name('user');

route::get('insertpersonal{infomation}/{email}/{firstname}/{lastname}/{dob}/{freelance}/{phone}/{address}/{lang}/{social}/{description}','Home@insertpersonal');

 Route::get('delperson{id}','Home@delperson');

  Route::get('insertexp{title}/{subtitle}/{date}/{descriptionexp}','Home@insertexp');

   Route::get('delexp{id}','Home@delperson');


Route::get('layout','Home@layout');




Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');



Route::get('homepagebk','Home@homebk');

