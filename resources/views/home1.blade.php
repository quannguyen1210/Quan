<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Quân - Creative vCard, Resume, CV</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.animatedheadline.css" />
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/skins/yellow.css" />

    <!-- Template JS Files -->
    <script src="js/modernizr.custom.js"></script>

</head>

<body class="dark">
    <!-- Preloader Start -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- Preloader Ends -->
    <!-- Wrapper Starts -->
    <div class="wrapper">
        <div id="bl-main" class="bl-main">
            <!-- Top Left Section Starts -->
            
            <!-- Top Left Section Ends -->
            <!-- About Section Starts -->
           @foreach($hometable as $info)
                @if(empty($info->txtbg))
                    <section class="topleft">
                        <div class="bl-box row valign-wrapper">
                            <div class="row valign-wrapper">
                                <div class="title-heading">
                                    <div class="selector uppercase" id="selector">
                                         <h3 class="ah-headline p-none m-none">
                                    
                                    <?php $info = json_decode($hometable,true) ;?>

                                    <span class="font-weight-300">{{$info[0]['hello']}}</span>
                                    <span class="ah-words-wrapper">
                                        <b class="is-visible">{{$info[0]['name']}}</b>
                                        @foreach($hometable as $value)
                                        <b>{{$value->job}}</b>
                                        @endforeach
                                    </span>

                                            </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </section>
                @else 
                <section>
                        <div class="bl-box valign-wrapper">
                            <div class="page-title center-align">
                            <span class="title-bg">{{$info->txtbg}}</span>
                            <h2 class="center-align">
                                <span data-hover="{{$info->firsttitle}}">{{$info->firsttitle}}</span>
                                <span data-hover="{{$info->lasttitle}}">{{$info->lasttitle}}</span>
                            </h2>
                        </div>
                    </div>


                
                <!-- About Title Ends -->
                <!-- About Expanded Starts -->
                
                <!-- End Expanded About -->



                <div class="bl-content">
                    <!-- Main Heading Starts -->
                    <div class="container page-title center-align">
                        <h2 class="center-align">
                            <span data-hover="{{$info->firsttitle}}">{{$info->firsttitle}} </span>
                            <span data-hover="{{$info->lasttitle}}">{{$info->lasttitle}}</span>
                        </h2>
                        <span class="title-bg">{{$info->txtbg}}</span>
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="container infos">
                        <!-- Divider Starts -->
                        <div class="divider center-align">
                            <span class="outer-line"></span>
                            <span class="fa fa-vcard" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                        <!-- Divider Ends -->
                        <!-- Personal Informations Starts -->
                        @foreach($personal as $person)
                        <div class="row">
                            <!-- Picture Starts -->
                            <div class="col s12 m5 l4 xl3 profile-picture">
                                <img src="images/photo-about.jpg" class="responsive-img my-picture" alt="My Photo">
                            </div>
                            <!-- Picture Ends -->
                            <div class="col s12 m7 l8 xl9 personal-info">
                                <h6 class="uppercase"><i class="fa fa-user"></i>{{$person->infomation}} </h6>
                                <div class="col m12 l7 xl7 p-none">
                                    <p class="second-font">I'm a Freelance Web Designer &amp; Developer based in Madrid, Spain.<br>
                                    {{$person->description}}
                                    </p>
                                </div>
                                <div class="col s12 m12 l6 p-none">
                                    <ul class="second-font list-1">
                                        <li><span class="font-weight-600">First Name: </span>{{$person->firstname}}</li>
                                        <li><span class="font-weight-600">Last Name: </span>{{$person->lastname}}</li>
                                        <li><span class="font-weight-600">Date of birth: </span>{{$person->dob}}</li>
                                        <li><span class="font-weight-600">Nationality: </span>{{$person->firstname}}</li>
                                        <li><span class="font-weight-600">Freelance: </span>{{$person->freelance}}</li>
                                    </ul>
                                </div>
                                <div class="col s12 m12 l6 p-none">
                                    <ul class="second-font list-2">
                                        <li><span class="font-weight-600">Phone: </span>{{$person->phone}}</li>
                                        <li><span class="font-weight-600">Address: </span>{{$person->address}}</li>
                                        <li><span class="font-weight-600">Email: </span>{{$person->email}}/li>
                                        <li><span class="font-weight-600">Spoken Langages: </span>{{$person->lang}}</li>
                                        <li><span class="font-weight-600">Social: </span>{{$person->social}}</li>
                                    </ul>
                                </div>
                                <a href="#" class="col s12 m12 l4 xl4 waves-effect waves-light btn font-weight-500">
                                    Download Resume <i class="fa fa-file-pdf-o"></i>
                                </a>
                                <a href="blog-dark.html" class="col s12 m12 l4 xl4 btn btn-blog font-weight-500">
                                    My Blog <i class="fa fa-edit"></i>
                                </a>
                            </div>
                        </div>
                        @endforeach
                        <!-- Personal Informations Ends -->
                    </div>
                    <!-- Resume Starts -->
                    <div class="resume-container">
                        <div class="container">
                            <div class="valign-wrapper row">
                                <!-- Resume Menu Starts -->

                                <div class="resume-list col l4">
                                    @foreach($exp as $experi)
                                    @if(!empty($experi->title))
                                    <div class="resume-list-item is-active" data-index="0" id="resume-list-item-0">
                                        <div class="resume-list-item-inner">
                                            <h6 class="resume-list-item-title uppercase"><i class="fa fa-briefcase"></i> {{$experi->title}}</h6>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    
                                </div>
                                <!-- Resume Menu Ends -->
                                <!-- Resume Content Starts -->
                                
                                <div class="col s12 m12 l8 resume-cards-container">
                                    <div class="resume-cards">
                                        <!-- Experience Starts -->
                                        @foreach($exp as $experi)
                                        @if(!empty($experi->title))
                                        <div class="resume-card resume-card-{{$experi->id}}" data-index="{{$experi->id}}">
                                            <!-- Experience Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-briefcase"></i> {{$experi->title}}</div>
                                            </div>
                                            <!-- Experience Header Title Ends -->
                                            <!-- Experience Content Starts -->
                                            <div class="resume-card-body experience">
                                                <div class="resume-card-body-container second-font">
                                                    <!-- Single Experience Starts -->
                                                    <span class="separator"></span>

                                                    <div class="resume-content">
                                                        <h6 class="uppercase"><span>Web Designer - </span>Envato</h6>
                                                        <span class="date"><i class="fa fa-calendar-o"></i>{{$experi->date}}</span>
                                                        <p>{{$experi->description}}</p>
                                                    </div>
                                                    <!-- Single Experience Ends -->
                                                    
                                                    <!-- Single Experience Starts -->
                                                  
                                                    <!-- Single Experience Ends -->
                                                    
                                                    <!-- Single Experience Starts -->
                                                    
                                                    <!-- Single Experience Ends -->
                                                </div>
                                            </div>
                                            <!-- Experience Content Starts -->
                                        </div>
                                        @endif
                                @endforeach
                                        <!-- Experience Ends -->
                                        <!-- Education Starts -->
                                        
                                        <!-- Education Ends -->
                                        <!-- Skills Starts -->
                                     
                                        <!-- Skills Ends -->
                                    </div>
                                </div>
                                
                                <!-- Resume Content Ends -->
                            </div>
                        </div>
                    </div>
                    <!-- Resume Ends -->
                    <!-- Fun Facts Starts -->
                    <div class="container badges">
                        <div class="row">
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                <h3>
                                    <i class="fa fa-suitcase"></i>
                                    <span class="font-weight-700">7+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Years Experience</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                <h3>
                                    <i class="fa fa-check-square"></i>
                                    <span class="font-weight-700">89+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Done Projects</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                            <!-- Fact Badge Item Starts -->
                            <div class="col s12 m4 l4 center-align">
                                 <h3>
                                    <i class="fa fa-heart"></i>
                                    <span class="font-weight-700">77+</span>
                                </h3>
                                <h6 class="uppercase font-weight-500">Happy customers</h6>
                            </div>
                            <!-- Fact Badge Item Ends -->
                        </div>
                    </div>
                    <!-- Fun Facts Ends -->
                </div>
                <img alt="close" src="images/close-button.png" class="bl-icon-close" />
            </section>
          @endif

            @endforeach


            <!-- About Ends -->
            <!-- Portfolio Starts -->
            
                <!-- Portfolio Navigation Ends -->
            </div>
            <!-- Portfolio Panel Items Ends -->
        </div>
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/jquery.animatedheadline.js"></script>
    <script src="js/boxlayout.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/jquery.hoverdir.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>