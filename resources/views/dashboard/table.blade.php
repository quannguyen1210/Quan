@extends('dashboard.layout')
@section('main')
<?php use App\Http\Controllers\Home ; ?> 

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Striped Table with Hover</h4>
                                <p class="category">Here is a subtitle for this table</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Hello</th>
                                    	<th>Name</th>
                                    	<th>Job</th>
                                    	<th>Txtbg</th>
                                        <th>FirstTitle</th>
                                        <th>LastTitle</th>
                                    </thead>
                                    <?php foreach ($hometable as  $info)  :  ?>
                                    <tbody>
                                        <tr>
                                        	
  <td> <?php echo $info->id ?> </td>
       
    <td>                                    
    <input class="input"   type="text" id="hello_<?php echo $info->id ?>"  value="<?php echo $info->hello ?>">
     </td> 
     <td>                                       
    <input class="input"   type="text" id="name_<?php echo $info->id ?>"  value="<?php echo $info->name ?>">
     </td>                         	
    <td> 
    <input  class="input"   type="text" id="job_<?php echo $info->id ?>"  value="<?php echo $info->job ?>">
    </td> 
        
    <td>                                 	
    <input class="input"   type="text" id="txtbg_<?php echo $info->id?>"  value="<?php echo $info->txtbg?>">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="firsttitle_<?php echo $info->id ?>"  value="<?php echo $info->firsttitle ?>">
    </td>
    
    <td>                                   	
    <input class="input"   type="text" id="lasttitle_<?php echo $info->id ?>"  value="<?php echo $info->lasttitle ?>">
     </td>                                                                         


    <td>  
        <button onclick="delete1(<?php echo  $info->id ;?>)">delete</button>
        <button onclick="update(<?php echo  $info->id ;?>)">update</button>

    </td>
    </tr>
    </tbody>
                                    <?php endforeach ;?>
    
    <td> 
    <input  class="input"   type="text" id="hello"  value="">
    </td> 
        
    <td>                                    
    <input class="input"   type="text" id="name"  value="">
    </td>

    <td> 
    <input  class="input"   type="text" id="job"  value="">
    </td> 
        
    <td>                                    
    <input class="input"   type="text" id="txtbg"  value="">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="firsttitle"  value="">
    </td>
    
    <td>                                    
    <input class="input"   type="text" id="lasttitle"  value="">
    </td>

    <td> 
    <button onclick="insert()">Insert</button>
    </td>
    
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                <h4 class="title">Personal</h4>
                                <p class="category">Here is a subtitle for this table</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover">
                                    <thead>
                                        <th>Infomation</th>
                                        <th>Email</th>
                                    	<th>First Name</th>
                                    	<th>Last Name</th>
                                    	<th>DOB</th>
                                    	<th>Freelance</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Lang</th>
                                        <th>Facebook</th>
                                        <th>Description</th>
                                    </thead>
                                    <?php foreach ($personal as  $info)  :  ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $info->infomation ?></td>
                                            <td><?php echo $info->email ?></td>
                                            <td><?php echo $info->firstname ?></td>
                                            <td><?php echo $info->lastname ?></td>
                                            <td><?php echo $info->dob ?></td>
                                            <td><?php echo $info->freelance ?></td>
                                            <td><?php echo $info->phone ?></td>
                                            <td><?php echo $info->address ?></td>
                                            <td><?php echo $info->lang ?></td>
                                            <td><?php echo $info->social ?></td>
                                            <td><?php echo $info->description ?></td>
                                     <td><button onclick="deletepersonal(<?php echo  $info->id ;?>)">delete</button></td>
         
                                        </tr>
    
                                    </tbody>
                                    <?php endforeach ;?>
    <tbody>
    <tr>

    <td> 
    <input  class="input"   type="text" id="infomation"  value="">
    </td> 

    <td>                                    
    <input class="input"   type="text" id="email"  value="">
    </td>
        
    <td>                                    
    <input class="input"   type="text" id="firstname"  value="">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="lastname"  value="">
    </td>
    
    <td>                                    
    <input class="input"   type="text" id="dob"  value="">
    </td>

     <td> 
    <input  class="input"   type="text" id="freelance"  value="">
    </td> 
        
    <td>                                    
    <input class="input"   type="text" id="phone"  value="">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="address"  value="">
    </td>
    
    <td>                                    
    <input class="input"   type="text" id="lang"  value="">
    </td>

    <td>                                    
    <input class="input"   type="text" id="social"  value="">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="description"  value="">
    </td>

    <td>
        <button onclick="insertpersonal()" >Insert</button>
    </td>

    </tr>
    
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                <h4 class="title">Exp</h4>
                                <p class="category">Here is a subtitle for this table</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover">
                                    <thead>
                                        <th>Title</th>
                                        <th>Subtitle</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                    </thead>
                                    <?php foreach ( $exp as  $info)  :  ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $info->title ?></td>
                                            <td><?php echo $info->subtitle ?></td>
                                            <td><?php echo $info->date ?></td>
                                            <td><?php echo $info->description ?></td>
                                     <td><button onclick="deleteexp(<?php echo  $info->id ;?>)">delete</button></td>
         
                                        </tr>
    
                                    </tbody>
                                    <?php endforeach ;?>
    <tbody>
    <tr>

    <td> 
    <input  class="input"   type="text" id="title"  value="">
    </td> 

    <td>                                    
    <input class="input"   type="text" id="subtitle"  value="">
    </td>
        
    <td>                                    
    <input class="input"   type="text" id="date"  value="">
    </td>
    
    <td>                                       
    <input class="input"   type="text" id="descriptionexp"  value="">
    </td>
    
    

    <td>
        <button onclick="insertexp()" >Insert</button>
    </td>

    </tr>
    
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@stop

<script type="text/javascript">
      function delete1(id)
      {
            var xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Xoa Thanh Cong'); 
                location.reload();
            }
            };
            xhttp.open("GET", "http://localhost:8000/delete"+id);
            xhttp.send();       
      }

      function update(id)
      {
            var xhttp = new XMLHttpRequest();
             

            var job = document.getElementById('job_'+id).value;
            alert(job);

             var txtbg = document.getElementById('txtbg_'+id).value;
             alert(txtbg);

             var firsttitle = document.getElementById('firsttitle_'+id).value;
             alert(firsttitle);

             var lasttitle = document.getElementById('lasttitle_'+id).value;
             alert(lasttitle);

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('update Thanh Cong'); 
                location.reload();
            }
            };
            

            xhttp.open("GET", "http://localhost:8000/updateinfo"+id+'/'+job+'/'+txtbg+'/'+firsttitle+'/'+lasttitle);
            xhttp.send();       
      }

      function insert()
      {
         
            var xhttp = new XMLHttpRequest();

             
            var hello = document.getElementById('hello').value;
            alert(hello);

            var name = document.getElementById('name').value;
            alert(name);


            var job = document.getElementById('job').value;
            alert(job);

             var txtbg = document.getElementById('txtbg').value;
             alert(txtbg);

             var firsttitle = document.getElementById('firsttitle').value;
             alert(firsttitle);

             var lasttitle = document.getElementById('lasttitle').value;
             alert(lasttitle);

             
            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Insert Thanh Cong'); 
                location.reload();
            }
            };
            

            xhttp.open("POST", "http://localhost:8000/insert");
            xhttp.setRequestHeader(
                "X-CSRF-TOKEN" ,
                 document.querySelector('meta[name="csrf-token"]').getAttribute('content'));
            xhttp.send("'hello'=hello&'name'=name&'job'=job&'txtbg'=txtbg&'firsttitle'=firsttitle&'lasttitle'=lasttitle");       
      }

       function deletepersonal(id)
      {
            var xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Xoa Thanh Cong'); 
                location.reload();
            }
            };
            xhttp.open("GET", "http://localhost:8000/delperson"+id);
            xhttp.send();       
      }

      function insertpersonal()
      {
            var xhttp = new XMLHttpRequest();
             

            var infomation = document.getElementById('infomation').value;
            alert(infomation);

            var email = document.getElementById('email').value;
            alert(email);

             var firstname = document.getElementById('firstname').value;
             alert(firstname);

             var lastname = document.getElementById('lastname').value;
             alert(lastname);

             var dob = document.getElementById('dob').value;
             alert(dob);

              var freelance = document.getElementById('freelance').value;
            alert(freelance);

             var phone = document.getElementById('phone').value;
             alert(phone);

             var address = document.getElementById('address').value;
             alert(address);

             var lang = document.getElementById('lang').value;
             alert(lang);

             var social = document.getElementById('social').value;
             alert(social);

             var description = document.getElementById('description').value;
             alert(description);

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Insert Thanh Cong'); 
                location.reload();
            }
            };
            

            xhttp.open("GET", "http://localhost:8000/insertpersonal"+infomation+'/'+email+'/'+firstname+'/'+lastname+'/'+dob+'/'+freelance+'/'+phone+'/'+address+'/'+lang+'/'+social+'/'+description);
            xhttp.send();       
      }


    function insertexp()
      {
            var xhttp = new XMLHttpRequest();
             

            var title = document.getElementById('title').value;
            alert(title);

             var subtitle = document.getElementById('subtitle').value;
             alert(subtitle);

             var date = document.getElementById('date').value;
             alert(date);

             var descriptionexp = document.getElementById('descriptionexp').value;
             alert(descriptionexp);

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Insert Thanh Cong'); 
                location.reload();
            }
            };
            

            xhttp.open("GET", "http://localhost:8000/insertexp"+title+'/'+subtitle+'/'+date+'/'+descriptionexp);
            xhttp.send();       
      }

      function deleteexp(id)
      {
            var xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function()
            {
              if (this.readyState == 4 && this.status == 200) {
                alert('Xoa Thanh Cong'); 
                location.reload();
            }
            };
            xhttp.open("GET", "http://localhost:8000/delexp"+id);
            xhttp.send();       
      }

      
    </script>




