<?php

namespace App\Http\Controllers;
use App\Info;
use App\HomeDelete;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Home extends Controller
{
	
	public function show($id)
	{
		
		return Info::find($id);
	}
	
	public function update($id, $name,$age,$address)
	
	{
		$update = Info::find($id);
		$update->name=$name;
    	$update->age=$age;
    	$update->address=$address;
    	$update->save();
    	
	}

	
	public function form()
	{
		echo view ('form');
	}

	public function admin()
	{
		$data = array();
		$data['row'] = Info::all();
		return view ('admin1')->with($data);
	}

	public function dashboard()
	{
		echo view('dashboard.dashboard');
	}

	public function infomation()
	{
		 $data = array();
		 $data['hometable'] = DB::table('hometable')->get();
		 $data['personal'] = DB::table('personal')->get();
		 $data['exp'] = DB::table('exp')->get();
		 

		 return view ('dashboard/table',$data);
		
	}

	public function layout()
	{
		return view('dashboard/layout');
	}

	public function index()
	{
		$data = array();
		$data['hometable'] = DB::table('hometable')->get();
		$data['personal'] = DB::table('personal')->get();
		$data['exp']     =  DB::table('exp')->get();

		return view ('home1',$data);
	}


	public function insert(Request  $rq) //insert giá trị tự chọn
	{

			$hello = $rq->input('hello');
			$name = $rq->input('name');
			$job = $rq->input('job');
			$txtbg = $rq->input('txtbg');
			$firsttitle = $rq->input('firsttitle');
			$lasttitle = $rq->input('lasttitle');

    		DB::table('hometable')->insert(
    		[	
    			'hello'=> $hello,
    			'name'=> $name,
    			'job'=> $job,
    			'txtbg'=> $txtbg,
    			'firsttitle'=> $firsttitle,
    			'lasttitle'=> $lasttitle
    		]);

		

	}

	public function updateinfo($id, $job,$txtbg,$firsttitle,$lasttitle)
	
	{
		DB::table('hometable')->where('id',$id)->update(
			[	
				'job'=> $job,
    			'txtbg'=> $txtbg,
    			'firsttitle'=> $firsttitle,
    			'lasttitle'=> $lasttitle
    		]);
    	
    	
	}

	public function delete($id)
	{

		DB::table('hometable')->where('id',$id)->delete();; // xóa dữ liệu theo cột age với tham số tự chọn
	}

	public function edituser()
	{
		$data = array();
		$data['personal'] = DB::table('personal')->get();

		return view('dashboard.edituser',$data);
	}
	
	public function insertpersonal($infomation,$email,$firstname,$lastname,$dob,$freelance,$phone,$address,$lang,$social,$description)
	{
		DB::table('personal')->insert(
		[
			'infomation'=> $infomation ,
			'email'=> $email  ,
    		'firstname'=> $firstname  ,
    		'lastname'=> $lastname,
    		'dob'=> $dob,
    		'freelance'=>  $freelance,
    		'address'=> $address,
    		'phone'=>  $phone,
    		'address'=>  $address,
    		'lang'=> $lang ,
    		'social'=>  $social,
    		'description'=> $description
			
		]);
	}

	public function delperson($id)
	{

		DB::table('personal')->where('id',$id)->delete();; // xóa dữ liệu theo cột age với tham số tự chọn
	}

	public function homebk()
	{
		return view('homepageBK');
	}

	public function insertexp($title,$subtitle,$date,$descriptionexp) //insert giá trị tự chọn
	{
			
    		DB::table('exp')->insert(
    		[	

    			'title'=> $title,
    			'subtitle'=> $subtitle,
    			'date'=> $date,
    			'description'=> $descriptionexp
    		]);

		

	}

	public function delexp($id)
	{

		DB::table('exp')->where('id',$id)->delete();; // xóa dữ liệu theo cột age với tham số tự chọn
	}
}


