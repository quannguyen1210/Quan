<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Personal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('personal', 255);
            $table->string('firstname', 60);
            $table->string('lastname', 64);
            $table->string('dob', 64);
            $table->string('nation', 64);
            $table->string('phone', 64);
            $table->string('address', 64);
            $table->string('lang', 64);
            $table->string('facebook', 64);
            $table->string('image', 64)->nullable();
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Personal');
    }
}
