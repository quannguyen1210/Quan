<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Home extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Hometable', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hello')->nullable();
            $table->string('name')->nullable();
            $table->string('job');
            $table->string('txtbg');
            $table->string('firsttitle');
            $table->string('lasttitle');
            $table->string('imageinfo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
